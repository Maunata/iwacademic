
<!DOCTYPE HTML>
<html lang="en-US">
<head>
 <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>signup </title>
 
 <script src="https://code.jquery.com/jquery-3.2.1.js"></script> 
</head>
<body>

<div class="container">      
            <form id="signup" >
               
    <div >
      
      <input type="text"  placeholder="Enter mobile number" id="form_mobile" name="mobile" required="">
    <span class="error_form" id="mobile_error_message"></span>
    </div>
    
    <div >
      
      <input type="text" placeholder="Enter first name" id="form_firstname" name="firstName" required="">
    <span class="error_form" id="firstName_error_message"></span>
    </div>
    
    <div >
      
      <input type="text" placeholder="Enter last name" id="form_lastname"name="lastName" required="">
    <span class="error_form" id="lastName_error_message"></span>
    </div>
    
    <div >
     
     <input type="email" placeholder="Enter email" id="form_email" name="email" required="">
    <span class="error_form" id="email_error_message"></span>
    </div>
    
    <div >
     
     <input type="password" placeholder="Enter password" id="form_password name="password" required="">
    <span class="error_form" id="password_error_message"></span>
    </div>
             
                
                <input class="btn btn-primary  btn-signin" type="submit" name="Signup">
                <input class="btn btn-secondry  btn-signin" type="Reset" name="Reset"> 
    
            </form><!-- /form -->
   </div>
             
        <script type="text/javascript">
 $(function() {
 
 $("#mobile_error_message").hide();
 $("#firstName_error_message").hide();
 $("#lastName_error_message").hide();
 $("#email_error_message").hide();
 $("#password_error_message").hide();
 
 var error_mobile=false;
 var error_firstname=false;
 var error_lastname=false;
 var error_email=false;
 var error_password=false;
    
 $("#form_mobile").focusout(function(){
 check_mobile();
 });
 
 $("#form_firstname").focusout(function(){
 check_firstname();
 });
 $("#form_lastname").focusout(function(){
 check_lastname();
 });
 $("#form_email").focusout(function(){
 check_email();
 });
 $("#form_password").focusout(function(){
 check_password();
 });
 
 function check_mobile(){
 var pattern = /^[0-9]*$/;
 var mobile = $("#form_mobile").val()
 if (pattern.test(mobile) && mobile !==''){
 $("#mobile_error_message").hide();
 $("#form_mobile").css("border-bottom","2px solid #34F458");
 } else {
 $("#mobile_error_message").html("should contain only mobile number");
 $("#mobile_error_message").show();
 $("#form_mobile").css("border-bottom","2px solid #F90A0A");
 error_mobile = true;
 }
 }
 
 function check_firstname(){
 var pattern = /^[0-9]*$/;
 var firstname = $("#form_firstname").val()
 if (pattern.test(firstname) && firstname !==''){
 $("#firstname_error_message").hide();
 $("#form_firstname").css("border-bottom","2px solid #34F458");
 } else {
 $("#firstname_error_message").html("should contain only alphabatic");
 $("#firstname_error_message").show();
 $("#form_firstname").css("border-bottom","2px solid #F90A0A");
 error_firstname = true;
 }
 }
 
 
 function check_lastname(){
 var pattern = /^[0-9]*$/;
 var lastname = $("#form_lastname").val()
 if (pattern.test(lastname) && lastname !==''){
 $("#lastname_error_message").hide();
 $("#form_lastname").css("border-bottom","2px solid #34F458");
 } else {
 $("#lastname_error_message").html("should contain only alphabatic ");
 $("#lastname_error_message").show();
 $("#form_lastname").css("border-bottom","2px solid #F90A0A");
 error_lastname = true;
 }
 }
 
 function check_email(){
 var pattern = /^[0-9]*$/;
 var email = $("#form_email").val()
 if (pattern.test(email) && email !==''){
 $("#email_error_message").hide();
 $("#form_email").css("border-bottom","2px solid #34F458");
 } else {
 $("#email_error_message").html("enter currect email");
 $("#email_error_message").show();
 $("#form_email").css("border-bottom","2px solid #F90A0A");
 error_email = true;
 }
 }
 
 function check_password(){
 var pattern = /^[0-9]*$/;
 var password = $("#form_password").val()
 if (pattern.test(password) && password !==''){
 $("#password_error_message").hide();
 $("#form_password").css("border-bottom","2px solid #34F458");
 } else {
 $("#password_error_message").html("enter password number");
 $("#password_error_message").show();
 $("#form_password").css("border-bottom","2px solid #F90A0A");
 error_password = true;
 }
 }
 
 $("#signup").submit(function(){
 error_mobile = false;
 error_firstname = false;
 error_lastname = false;
 error_email = false;
 error_password = false;
 
 check_mobile();
 check_firstname();
 check_lastname();
 check_email()
 check_password();
 
 if (error_mobile === false && error_firstname === false && error_lastname === false && error_email === false && error_password === false) {
 alert ("registration successfull")
 return true;
 }
 else {
 alert ("please fill the form correctly)
 return false;
 }
 });
 });
 

 </script>
</body>
</html>